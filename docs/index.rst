.. distem documentation master file, created by
   sphinx-quickstart on Thu Sep 21 21:45:39 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Distem's documentation !
===================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   tutorials/index.rst
   apidoc/index.rst
