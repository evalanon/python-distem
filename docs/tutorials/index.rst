.. distem documentation master file, created by
   sphinx-quickstart on Thu Sep 21 21:45:39 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Tutorial
========

This tutorial show how the use of Distem, a provider that prepare containers for you on Grid'5000.

Prerequisites
-------------

First of all, you have to reserve some nodes on Grid'5000, deploy a system on your nodes.

.. code-block:: bash

    $ oarsub -t deploy -l "slash_22=1+nodes=2, walltime=02:00:00" -I
    $ kadeploy3 -f $OAR_NODE_FILE -e debian9-x64-nfs -k

Then use the distem bootstrap:

.. code-block:: bash

    $ distem-bootstrap --debian-version stretch

Distem-bootstrap installs Distem on the physical nodes and also starts a coordinator.

After the reservation done, you can get virtual IPs for your reservation :

.. code-block:: bash

    $ frontend> g5k-subnets -sp

You will need this address for the experiment.

You have to download the image to create new containers for the experiment.

.. code-block:: bash

    $ frontend> wget 'http://public.nancy.grid5000.fr/~amerlin/distem/distem-fs-jessie.tar.gz' -P ~/distem_img

It will create a new directory on your home with the jessie image.


Initialisation
--------------

To process to any experiments, you need to initialize nodes as the folowing code.

.. literalinclude:: scp_rsync/initialisation.py
    :language: python
    :linenos:

Experiment
----------

The experiment can be launched from the coordinator node as root.
Below, the python code of the experiment.

.. literalinclude:: scp_rsync/experiment.py
    :language: python
    :linenos:
