import time
import os
from statistics import mean, stdev

from distem import Distem


if __name__ == "__main__":

    # List of nodes
    nodelist = ['node-1', 'node-2']
    
    # List of latencies for the experiment
    latencies = ['0ms', '20ms', '40ms', '60ms']

    # Dictionnary for the experiment 
    results = {
        'scp': {},
        'rsync': {}
    }
    node1 = {
        'name': nodelist[0],
        'address': []
    }
    node2 = {
        'name': nodelist[1],
        'address': []
    }
    
    # Name of interface
    ifname = 'if0'

    # Number of iteration for the experiment
    iteration = 5

    # Connect to the Distem server (on http://localhost:4567 by default)
    distem = Distem()

    # Getting the -automatically affected- address of each virtual nodes
    # virtual network interfaces
    node1['address'] = distem.viface_info(node1['name'], ifname)['address'].split("/")[0]
    node2['address'] = distem.viface_info(node2['name'], ifname)['address'].split("/")[0]

    # Creating the files we will use in our experimentation
    distem.vnode_execute(node1['name'],
                         'mkdir -p /tmp/src ; cd /tmp/src ; \
                         for i in `seq 1 100`; do \
                         dd if=/dev/zero of=$i bs=1K count=50; \
                         done'
                         )

    # Printing the current latency
    start_time = time.time()
    distem.vnode_execute(node1['name'], 'hsjvdjq')
    print("Latency without any limitations # %s" %(str(time.time() - start_time)))

    desc = {
        'output': {
            'latency': {
                'delay': []
            }
        }
    }
    # Starting our experiment for each specified latencies
    print('Starting tests')
    for latency in latencies:
        results['scp'][latency] = []
        results['rsync'][latency] = []

        print("Latency #%s" %str(latency))
        # Update the latency description on virtual nodes
        desc['output']['latency']['delay'] = latency
        distem.viface_update(node1['name'], ifname, desc)
        distem.viface_update(node2['name'], ifname, desc)

        for i in range(iteration):
            print("\tIteration ## %s" %i)
            # Launch SCP test
            # Cleaning target directory on node2
            distem.vnode_execute(node2['name'], 'rm -rf /tmp/dst')
            # Starting the copy from node1 to node2
            start_time = time.time()
            cmd = "scp -rq /tmp/src %s:/tmp/dst" % node2["address"]
            distem.vnode_execute(node1['name'],
                                 cmd,
                                 )
            end_time = time.time()
            import ipdb; ipdb.set_trace()
            results['scp'][latency].append(time.time() - start_time)
            # Launch RSYNC test
            # Cleaning target directory on node2
            distem.vnode_execute(node2['name'], 'rm -rf /tmp/dst')
            # Starting the copy from node1 to node2
            start_time = time.time()
            distem.vnode_execute(node1['name'],
                                 "rsync -r /tmp/src #{node2['address']}:/tmp/dst"
                                 )
            results['rsync'][latency].append(time.time() - start_time)

    values = []
    print("Rsync results :")
    for latency in latencies:
        print(results['rsync'][latency])
        print("%s: [average=%s,standard_deviation=%s]"
              %(latency, mean(results['rsync'][latency]), stdev(results['rsync'][latency])))

    values = []
    print("Scp results :")
    for latency in latencies:
        print(results['scp'][latency])
        print("%s: [average=%s,standard_deviation=%s]"
              %(latency, mean(results['scp'][latency]), stdev(results['scp'][latency])))
