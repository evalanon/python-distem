import time
import os
from statistics import mean, stdev

from distem import Distem


if __name__ == "__main__":

    # The path to the compressed filesystem image
    # We can point to local file since our homedir is available from NFS
    FSIMG = "file:///home/USER/public/distem-fs-jessie.tar.gz"
    # Put the physical machines that have been assigned to you
    # You can get that by executing: cat $OAR_NODE_FILE | uniq
    PNODES = ["pnode1", "pnode2"]

    # The first argument of the script is the address (in CIDR format)
    # of the virtual network to set-up in our platform
    # This ruby hash table describes our virtual network
    vnet = {
        'name': 'testnet',
        'address': ARGV[0]
    }
    nodelist = ['node-1', 'node-2']
    # Read SSH keys
    PRIV_KEY = os.path.join(os.environ["HOME"], ".ssh", "id_rsa")
    PUB_KEY = "%s.pub" % PRIV_KEY

    private_key = open(os.path.expanduser(PRIV_KEY)).read()
    public_key = open(os.path.expanduser(PUB_KEY)).read()

    sshkeys = {
        "public" : public_key,
        "private" : private_key
    }
    node1 = {
        'name': nodelist[0],
        'address': []
    }
    node2 = {
        'name': nodelist[1],
        'address': []
    }
    ifname = 'if0'

    # Connect to the Distem server (on http://localhost:4567 by default)
    distem = Distem()

    # Start by creating the virtual network
    distem.vnetwork_create(vnet['name'],
                           vnet['address'])
    print("Network created")
    # Creating one virtual node per physical one
    # Create the first virtual node and set it to be hosted on
    # the first physical machine
    distem.vnode_create(nodelist[0],
                        {'host': PNODES[0]}, sshkeys)
    print("Node %s created" %nodelist[0])
    # Specify the path to the compressed filesystem image
    # of this virtual node
    distem.vfilesystem_create(nodelist[0],
                              {'image': FSIMG})
    # Create a virtual network interface and connect it to vnet
    distem.viface_create(nodelist[0],
                         'if0',
                         {'vnetwork': vnet['name'], 'default': 'true'})
    print("Interface created")
    # Create the first virtual node and set it to be hosted on
    # the second physical machine
    distem.vnode_create(nodelist[1],
                        {'host': PNODES[1]},
                        sshkeys)
    print("Node %s created" %nodelist[1])
    distem.vfilesystem_create(nodelist[1],
                              {'image': FSIMG})

    # Create a virtual network interface and connect it to vnet
    distem.viface_create(nodelist[1],
                         'if0',
                         {'vnetwork': vnet['name']})
    print("Interface created")
    # Starting the virtual nodes using the synchronous method
    distem.vnodes_start(nodelist)
    print("Nodes started")
